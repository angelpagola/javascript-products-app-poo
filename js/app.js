class Product {
    constructor(code, name, price, year) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.year = year;
    }
}

class UI {
    addProduct(product) {

        if (localStorage.getItem('products') === null) {
            let products = [];
            products.push(product);
            localStorage.setItem('products', JSON.stringify(products));
        } else {
            let products = JSON.parse(localStorage.getItem('products'));
            products.push(product);
            localStorage.setItem('products', JSON.stringify(products));
        }

        this.getProducts();
        this.resetForm();
    }
    getProducts() {
        let products = JSON.parse(localStorage.getItem('products'));
        const element = document.getElementById('product-list');

        element.innerHTML = '';
        element.innerHTML = `
        <tr>
            <th>Product Code</th>
            <th>Product Name</th>
            <th>Producto Price</th>
            <th>Product Year</th>
            <th>Update</th>
        </tr> 
        `;

        for (let i = 0; i < products.length; i++) {
            let productcode = products[i].code;
            let productname = products[i].name;
            let productprice = products[i].price;
            let productyear = products[i].year;
            element.innerHTML += '<tr>'
            element.innerHTML += `
            <td>${productcode}</td>
            <td>${productname}</td>
            <td>${productprice}</td>
            <td>${productyear}</td>
            <td><a href="#" name="delete" class='btn btn-danger' onclick="deleteProduct('${productcode}')" >x</a></td>
        `;
            element.innerHTML += '</tr>'

        }

    }
    resetForm() {
        document.getElementById('product-form').reset();
    }

    showMessage(message, cssClass) {
        const div = document.createElement('div')
        div.className = `alert alert-${cssClass} mt-4`;
        div.appendChild(document.createTextNode(message));
        //Showing in DOM
        const container = document.querySelector('.container');
        const app = document.querySelector('#App');
        //Elemento div antes de app y despues del container
        container.insertBefore(div, app);
        setTimeout(() => {
            document.querySelector('.alert').remove();
        }, 3000);
    }
}

function deleteProduct(code) {
    let products = JSON.parse(localStorage.getItem('products'));
    for (let i = 0; i < products.length; i++) {
        if (products[i].code == code) {
            products.splice(i, 1);
        }
    }
    localStorage.setItem('products', JSON.stringify(products));
    const ui = new UI();
    ui.getProducts();
    ui.showMessage('Product Deleted Successfully', 'danger');
}

document.getElementById("product-form").addEventListener("submit", (e) => {
    const code = document.getElementById('code').value;
    const name = document.getElementById('name').value;
    const price = document.getElementById('price').value;
    const year = document.getElementById('year').value;

    const product = new Product(code, name, price, year);
    const ui = new UI();
    if (code === '' || name === '' || price === '' || year === '') {
        //Return es para terminar la funcion
        return ui.showMessage('Complete Fields Please', 'info');
    }

    ui.addProduct(product);
    ui.showMessage('Product Added Successfully', 'success');

    //cancelar para que no se autoarefresque
    e.preventDefault();
});

const ui = new UI();
ui.getProducts();

/*document.getElementById('product-list').addEventListener('click', (e) => {
    const ui = new UI();
    ui.deleteProduct(e.target)
})
*/